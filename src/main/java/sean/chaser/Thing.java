package sean.chaser;

import sean.chaser.reference.Reference;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * A Thing is the top-level thing that contains references to other things, that are not necessarily {@link Thing}s
 */
public class Thing {

    private Handle handle;

    private Map<String, Set<Reference>> references;

    public Thing(Handle handle) {
        this.handle = handle;
    }

    public Handle getHandle() {
        return handle;
    }

    public Map<String, Set<Reference>> getReferences() {
        return references;
    }

    public void addReference(String refNature, Reference reference){
        // init the set and assign
    }

    public class Handle {
        private String id;
        private String type;

        public Handle(String id, String type) {
            this.id = id;
            this.type = type;
        }

        public String getId() {
            return id;
        }

        public String getType() {
            return type;
        }
    }
}
