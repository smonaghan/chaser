package sean.chaser;

import sean.chaser.reference.Reference;

public class ThingReference implements Reference {

    Thing.Handle handle;

    public ThingReference(Thing.Handle handle) {
        this.handle = handle;
    }

    @Override
    public String getPath() {
        return null;
    }
}
