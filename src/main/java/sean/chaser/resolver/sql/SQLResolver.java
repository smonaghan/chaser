package sean.chaser.resolver.sql;

import sean.chaser.reference.SQLReference;
import sean.chaser.resolver.Resolver;

public class SQLResolver implements Resolver<SQLResultSetPromise, SQLReference> {

    @Override
    public SQLResultSetPromise resolve(SQLReference reference) {
        Dao dao = new HSQLDao(reference.getConnectionString(), null);
        return new SQLResultSetPromise(dao, reference.getQuery());
    }
}
