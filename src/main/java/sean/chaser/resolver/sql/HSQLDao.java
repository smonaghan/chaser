package sean.chaser.resolver.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class HSQLDao implements Dao {

    public static final String HSQLDB_DRIVER = "org.hsqldb.jdbc.JDBCDriver";

    private String connectionString;

    private Connection connection;

    private Credentials credentials;

    public HSQLDao(String connectionString, Credentials credentials) {
        this.connectionString = connectionString;
        this.credentials = credentials;
    }

    @Override
    public void open() throws SQLException {
        connection = DriverManager.getConnection(connectionString, credentials.username, credentials.password);
    }

    @Override
    public void close() throws SQLException {
        connection.close();
    }

    @Override
    public ResultSet select(String sql) {
        return null;
    }
}
