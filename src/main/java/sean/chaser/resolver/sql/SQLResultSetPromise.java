package sean.chaser.resolver.sql;

import sean.chaser.resolver.Promise;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLResultSetPromise implements Promise<ResultSet> {

    private Dao dao;

    private String query;

    public SQLResultSetPromise(Dao dao, String query) {
        this.dao = dao;
        this.query = query;
    }

    @Override
    public ResultSet fulfill() {
        try {
            dao.open();
            return dao.select(query);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                dao.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}
