package sean.chaser.resolver.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface Dao {

    void open() throws SQLException;

    void close() throws SQLException;

    ResultSet select(String sql);

    class Credentials{
        public String username;
        public String password;
    }
}
