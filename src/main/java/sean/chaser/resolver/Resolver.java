package sean.chaser.resolver;

public interface Resolver<T, R> {

    T resolve(R reference);
}
