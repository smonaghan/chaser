package sean.chaser.resolver;

import sean.chaser.reference.Reference;

import java.util.*;

public class ResolverRegistry {

    private Map<String, Set<Resolver>> resolvers;

    public ResolverRegistry() {
        resolvers = new HashMap<String, Set<Resolver>>();
    }

    public Collection<Resolver> getReferenceResolvers(Reference reference) {
        return resolvers.get(reference.getClass());
    }

    public void register(Reference reference, Resolver resolver) {
        Set<Resolver> resolverSet = getResolverSet(reference);
        resolverSet.add(resolver);
    }

    private Set<Resolver> getResolverSet(Reference reference) {
        Set<Resolver> resolverSet = resolvers.get(reference.getClass());
        if (resolverSet == null) {
            resolverSet = new HashSet<Resolver>();
            resolvers.put(reference.getClass().toString(), resolverSet);
        }

        return resolverSet;
    }
}
