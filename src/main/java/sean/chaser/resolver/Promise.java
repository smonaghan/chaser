package sean.chaser.resolver;

public interface Promise <T>{

    T fulfill();
}
