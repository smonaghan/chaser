package sean.chaser.reference;

/**
 * A reference is a descriptor about a resource
 */
public interface Reference {

    String getPath();
}
