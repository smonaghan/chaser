package sean.chaser.reference;

public class SQLReference {

    String connectionString;

    String query;

    public SQLReference(String connectionString, String query) {
        this.connectionString = connectionString;
        this.query = query;
    }

    public String getConnectionString() {
        return connectionString;
    }

    public String getQuery() {
        return query;
    }
}
